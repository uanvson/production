﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Production.DataAccess;
using Production.Business.Utilities;
using Dapper;
using MvcPaging;

namespace Production.Business
{
    public class CropBL
    {
        public static List<Crop> GetList()
        {
            using (var conn = Commons.DBContext)
            {
                return conn.Query<Crop>("SELECT * FROM Crop").ToList();
            }
        }

        public IPagedList<Crop> GetList(Crop model = null)
        {
            model = model ?? new Crop();
            using (var conn = Commons.DBContext) {
                string sqlWhere = "1=1";
                string sqlOrder = " CropID DESC";
                if (!string.IsNullOrEmpty(model.CropName))
                    sqlWhere += " AND CropName like '%' + @CropName+ '%'";
                if (!string.IsNullOrEmpty(model.SortBy))
                    sqlOrder = model.SortBy + " " + model.SortDirection;
                return conn.QueryPaging<Crop>(
                    "Crop.*,CustomerName",
                    "Crop LEFT JOIN Customer ON Crop.CustomerID = Customer.CustomerID",
                    sqlWhere,
                    sqlOrder,
                    model, model.Page - 1);

            }
        }
        public Crop GetById(int CropID)
        {
            using (var conn = Commons.DBContext) {
                return conn.Query<Crop>(
                    "SELECT * FROM Crop WHERE CropID=@CropID",
                    new { CropID })
                    .DefaultIfEmpty(new Crop())
                    .FirstOrDefault();
            }
        }
        public Crop Save(Crop model)
        {
            using (var conn = Commons.DBContext) {
                string sql = "";
                if (model.CropID > 0) {
                    sql = @"
                        UPDATE [Crop]
                        SET CropName	    = @CropName		
                           ,CropCode	    = @CropCode		
                           ,InternalCode	= @InternalCode	
                           ,CustomerID      = @CustomerID
                        WHERE CropID = @CropID
                        SELECT * FROM Crop WHERE CropID =  @CropID
                    ";
                }
                else {
                    sql = @"
                        INSERT INTO [Crop]
                           ([CropName]
                           ,[CropCode]
                           ,[InternalCode]
                           ,CustomerID
                         )
                     VALUES
                           (@CropName		
                           ,@CropCode
                           ,@InternalCode
                           ,@CustomerID
                          ) 
                        SELECT * FROM Crop WHERE CropID = SCOPE_IDENTITY()
                    ";
                }
                model = conn.Query<Crop>(sql, model).FirstOrDefault();
                return model;
            }
        }
        public void Delete(int CropID)
        {
            using (var conn = Commons.DBContext) {
                conn.Execute(
                    "DELETE Crop WHERE CropID=@CropID ",
                    new { CropID });
            }
        }
    }
}
