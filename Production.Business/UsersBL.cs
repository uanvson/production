﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Production.DataAccess;
using Production.Business.Utilities;
using Dapper;
using MvcPaging;

namespace Production.Business
{
    public class UsersBL
    {
        public static List<Users> GetList()
        {
            using (var conn = Commons.DBContext)
            {
                return conn.Query<Users>("SELECT * FROM Users").ToList();
            }
        }        

        public IPagedList<Users> GetList(Users model = null)
        {
            model = model ?? new Users();
            using (var conn = Commons.DBContext) {
                string sqlWhere = "1=1";
                string sqlOrder = " Users.UserID DESC";
                if (!string.IsNullOrEmpty(model.FirstName))
                    sqlWhere += " AND Users.FirstName like '%' + @FirstName+ '%'";
                if (!string.IsNullOrEmpty(model.SortBy))
                    sqlOrder = model.SortBy + " " + model.SortDirection;
                return conn.QueryPaging<Users>(
                    "Users.*, RoleName,CustomerName",
                    @"Users LEFT JOIN Role ON Users.RoleID = Role.RoleID
                     LEFT JOIN Customer ON Users.CustomerID = Customer.CustomerID",
                    sqlWhere,
                    sqlOrder,
                    model, model.Page - 1);

            }
        }
        public Users GetById(int UserID)
        {
            using (var conn = Commons.DBContext) {
                return conn.Query<Users>(
                    @"SELECT Users.*, RoleName, CustomerName
                    FROM Users 
                    LEFT JOIN Role ON Users.RoleID = Role.RoleID
                    LEFT JOIN Customer ON Users.CustomerID = Customer.CustomerID
                    WHERE UserID=@UserID",
                    new { UserID })
                    .DefaultIfEmpty(new Users())
                    .FirstOrDefault();
            }
        }
        public Users Save(Users model)
        {
            using (var conn = Commons.DBContext) {
                string sql = "";
                if (model.UserID > 0) {
                    sql = @"
                        UPDATE [Users]
                        SET FirstName	= @FirstName		
                           ,LastName	= @LastName		
                           ,Email		= @Email
                           ,Phone       = @Phone
                           ,RoleID	    = @RoleID			
                           ,CustomerID  = @CustomerID			
                           
                        WHERE UserID = @UserID
                        SELECT * FROM Users WHERE UserID =  @UserID
                    ";
                }
                else {
                    sql = @"
                        INSERT INTO [Users]
                           ([FirstName]
                           ,[LastName]
                           ,[Email]
                           ,Phone
                           ,[RoleID]
                           ,[CustomerID])
                     VALUES
                           (@FirstName		
                           ,@LastName		
                           ,@Email	
                           ,@Phone 
                           ,@RoleID			
                           ,@CustomerID) 
                        SELECT * FROM Users WHERE UserID = SCOPE_IDENTITY()
                    ";
                }
                model = conn.Query<Users>(sql, model).FirstOrDefault();
                return model;
            }
        }
        public void Delete(int UserID)
        {
            using (var conn = Commons.DBContext) {
                conn.Execute(
                    "DELETE Users WHERE UserID=@UserID ",
                    new { UserID });
            }
        }
        public Users GetByEmail(string Email)
        {
            using (var conn = Commons.DBContext) {
                return conn.Query<Users>(
                    @"SELECT Users.*, RoleName, CustomerName
                    FROM Users 
                    LEFT JOIN Role ON Users.RoleID = Role.RoleID
                    LEFT JOIN Customer ON Users.CustomerID = Customer.CustomerID
                    WHERE Users.Email=@Email",
                    new { Email })
                    .FirstOrDefault();
            }
        }
    }
}
