﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Production.DataAccess;
using System.Data.SqlClient;
using Production.Business.Utilities;
using Dapper;

namespace Production.Business
{
    public class ResourceBL
    {
        public static string GetText(string key, int? languageID = null)
        {
            if (languageID == null)
                languageID = SessionUtilities.CurrentLanguageID;

            bool autoCreate = true;
            if (autoCreate){
                GetResource(key, languageID.GetValueOrDefault(), true);
            }
            
            string resourceKey = string.Format("Resource_{0}",languageID);
            Dictionary<string, Resource> resources;
            resources = (Dictionary<string, Resource>)CachingUtilities.GetCachingValue(resourceKey);
            if (resources == null)
            {
                var list = GetResources(languageID:languageID);
                resources = list.ToDictionary(x => x.ResourceKey, x => x);
                CachingUtilities.SetCachingValue(resourceKey, resources);
            }
            if (resources.Keys.Contains(key))
                return resources[key].Value;
            else
                return $"[{key}]";
        }

        public static List<Resource> GetResources(string key="", int? languageID = 1)
        {
            using (var conn = new SqlConnection(GlobalVariables.DBConnectionString))
            {
                string sql = @"SELECT * FROM Resource WHERE 1=1";
                if (languageID > 0)
                    sql += " AND LanguageID = @LanguageID";
                if (!string.IsNullOrEmpty(key))
                    sql += " AND ResourceKey = @ResourceKey";

                return conn.Query<Resource>(sql, new { languageID, ResourceKey = key }).DefaultIfEmpty(new Resource() { ResourceKey="fake"}).ToList();
            }
        }

        public static Resource GetResource(string key, int languageID = 1, bool autoCreate = false)
        {
            using (var conn = new SqlConnection(GlobalVariables.DBConnectionString))
            {
                Resource result;
                string sql = @"SELECT * FROM Resource 
                                WHERE LanguageID = @LanguageID AND ResourceKey = @ResourceKey";
                result = conn.Query<Resource>(sql, new { languageID, ResourceKey = key }).FirstOrDefault();

                if (autoCreate && result == null)
                {
                    sql = @"INSERT INTO [Resource]
                           ([ResourceKey]
                           ,[LanguageID]
                           ,[Value])
                        VALUES
                           (@ResourceKey
                           ,@LanguageID
                           ,@Value)";
                    conn.Execute(sql, new { ResourceKey = key, languageID, Value = key });

                    string resourceKey = string.Format("Resource_{0}", languageID);
                    CachingUtilities.RemoveCache(resourceKey);

                }

                return result;



            }
        }


    }
}
