﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Production.Business.Utilities;
using Production.DataAccess;

namespace Production.Business
{
    public class CustomerBL
    {
        public List<Customer> GetList(Customer model = null)
        {
            model = model ?? new Customer();
            using (var conn = Commons.DBContext)
            {
                string sql = "SELECT * FROM Customer";
                if (!string.IsNullOrEmpty(model.CustomerName))
                    sql += " WHERE CustomerName like '%' + @CustomerName+ '%'";
                if (!string.IsNullOrEmpty(model.SortBy))
                    sql += " ORDER BY " + model.SortBy + " " + model.SortDirection;
                return conn.Query<Customer>(sql, model).ToList();
            }
        }
        public Customer GetById(int CustomerID)
        {
            using (var conn = Commons.DBContext) {
                return conn.Query<Customer>(
                    "SELECT * FROM Customer WHERE CustomerID=@CustomerID",
                    new { CustomerID})
                    .DefaultIfEmpty(new Customer())
                    .FirstOrDefault();
            }
        }
        public Customer Save(Customer model)
        {
            using (var conn = Commons.DBContext) {
                string sql = "";                
                if (model.CustomerID > 0) {
                    sql = @"
                        UPDATE [Customer]
                           SET [CustomerName] = @CustomerName
                              ,[CustomerCode] = @CustomerCode
                              ,[Phone] = @Phone
                              ,[Email] = @Email
                              ,[FirstName] = @FirstName
                              ,[LastName] = @LastName
                              ,[Address] = @Address
                              ,[CountryID] = @CountryID
                        WHERE CustomerID = @CustomerID
                        SELECT * FROM Customer WHERE CustomerID = @CustomerID
                    ";
                }
                else {
                    sql = @"
                        INSERT INTO [Customer]
                               ([CustomerName]
                               ,[CustomerCode]
                               ,[Phone]
                               ,[Email]
                               ,[FirstName]
                               ,[LastName]
                               ,[Address]
                               ,[CountryID])
                         VALUES
                               (@CustomerName
                               ,@CustomerCode
                               ,@Phone
                               ,@Email
                               ,@FirstName
                               ,@LastName
                               ,@Address
                               ,@CountryID) 
                        SELECT * FROM Customer WHERE CustomerID = SCOPE_IDENTITY()
                    ";
                }
                model = conn.Query<Customer>(sql, model).FirstOrDefault();
                return model;
            }
        }
        public void Delete(int CustomerID)
        {
            using (var conn = Commons.DBContext) {                
                conn.Execute(
                    "DELETE Customer WHERE CustomerID=@CustomerID ", 
                    new { CustomerID });
            }
        }
    }
}
