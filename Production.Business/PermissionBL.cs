﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Production.DataAccess;
using Production.Business.Utilities;
using MvcPaging;

namespace Production.Business
{
    public class PermissionBL
    {
        static public List<Permission> GetList()
        {
            using (var conn = Commons.DBContext)
            {
                return conn.Query<Permission>("SELECT * FROM Permission").ToList();
            }
        }

        public IPagedList<Permission> GetList(Permission model = null)
        {
            model = model ?? new Permission();
            using (var conn = Commons.DBContext) {
                string sqlWhere = "1=1";
                string sqlOrder = " PermissionID DESC";
                if (!string.IsNullOrEmpty(model.PermissionName))
                    sqlWhere += " AND PermissionName like '%' + @PermissionName+ '%'";
                if (!string.IsNullOrEmpty(model.SortBy))
                    sqlOrder =  model.SortBy + " " + model.SortDirection;
                return conn.QueryPaging<Permission>(
                    "*",
                    @"Permission",
                    sqlWhere,
                    sqlOrder,
                    model, model.Page-1);

            }
        }
        public List<Permission> GetRolePermissions(int RoleID)
        {
            using (var conn = Commons.DBContext) {
                return conn.Query<Permission>(
                    @"SELECT * 
                    FROM  Permission 
                    JOIN  RolePermission rp ON Permission.PermissionID = rp.PermissionID 
                    WHERE RoleID = @RoleID
                    ORDER BY PermissionName ASC ",
                    new { RoleID }).ToList();

            }
        }
        public Permission GetById(int PermissionID)
        {
            using (var conn = Commons.DBContext) {
                return conn.Query<Permission>(
                    "SELECT PermissionID OlPermissionID,* FROM Permission WHERE PermissionID=@PermissionID",
                    new { PermissionID })
                    .DefaultIfEmpty(new Permission())
                    .FirstOrDefault();
            }
        }
        public Permission Save(Permission model)
        {
            using (var conn = Commons.DBContext) {
                string sql = "";
                if (model.OldPermissionID > 0) {
                    sql = @"                   
                        UPDATE [Permission]
                        SET PermissionName	= @PermissionName		
                           ,Description	    = @Description
                        WHERE PermissionID  = @PermissionID
                        SELECT * FROM Permission WHERE PermissionID =  @PermissionID
                    ";
                }
                else {
                    sql = @"
                        IF EXISTS(SELECT 1 FROM Permission WHERE PermissionID = @PermissionID)
                            SELECT -1 PermissionID
                        ELSE
                        BEGIN                        
                            IF (@PermissionID = 0)
                                SELECT @PermissionID = ISNULL(MAX(PermissionID),0) + 1 FROM Permission
                        
                            INSERT INTO [Permission]
                               (PermissionID
                               ,[PermissionName]
                               ,[Description])
                            VALUES
                               (@PermissionID
                               ,@PermissionName		
                               ,@Description)
                            SELECT * FROM Permission WHERE PermissionID = @PermissionID
                        END
                    ";
                }
                model = conn.Query<Permission>(sql, model).FirstOrDefault();
                return model;
            }
        }
        public void Delete(int PermissionID)
        {
            using (var conn = Commons.DBContext) {
                conn.Execute(
                    "DELETE Permission WHERE PermissionID=@PermissionID ",
                    new { PermissionID });
            }
        }

    }
}
