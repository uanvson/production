﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Production.DataAccess;
using Production.Business.Utilities;
using Dapper;
using MvcPaging;

namespace Production.Business
{
    public class UnitBL
    {
        public static List<Unit> GetList()
        {
            using (var conn = Commons.DBContext)
            {
                return conn.Query<Unit>(
                    @"SELECT *, FirstName, LastName 
                    FROM Unit 
                    LEFT JOIN Users u ON u.UserID = Unit.OwnerID

                ").ToList();
            }
        }

        public IPagedList<Unit> GetList(Unit model = null)
        {
            model = model ?? new Unit();
            using (var conn = Commons.DBContext) {
                string sqlWhere = "1=1";
                string sqlOrder = " Unit.UnitID DESC";
                if (!string.IsNullOrEmpty(model.UnitName))
                    sqlWhere += " AND UnitName like '%' + @UnitName+ '%'";
                if (!string.IsNullOrEmpty(model.SortBy))
                    sqlOrder = model.SortBy + " " + model.SortDirection;
                return conn.QueryPaging<Unit>(
                    "Unit.*, FirstName, LastName",
                    "Unit LEFT JOIN Users ON Unit.OwnerID = Users.UserID",
                    sqlWhere,
                    sqlOrder,
                    model, model.Page - 1);

            }
        }
        public Unit GetById(int UnitID)
        {
            using (var conn = Commons.DBContext) {
                return conn.Query<Unit>(
                    "SELECT * FROM Unit WHERE UnitID=@UnitID",
                    new { UnitID })
                    .DefaultIfEmpty(new Unit())
                    .FirstOrDefault();
            }
        }
        public Unit Save(Unit model)
        {
            using (var conn = Commons.DBContext) {
                string sql = "";
                if (model.UnitID > 0) {
                    sql = @"
                        UPDATE [Unit]
                        SET UnitName	= @UnitName		
                           ,UnitNumber	= @UnitNumber		
                           ,Area		= @Area	
                           ,HouseNumber = @HouseNumber	
                           ,FarmID      = @FarmID
                           ,OwnerID     = @OwnerID
                        WHERE UnitID = @UnitID
                        SELECT * FROM Unit WHERE UnitID =  @UnitID
                    ";
                }
                else {
                    sql = @"
                        INSERT INTO [Unit]
                           ([UnitName]
                           ,[UnitNumber]
                           ,[Area]  
                           ,FarmID
                           ,[OwnerID])
                     VALUES
                           (@UnitName		
                           ,@UnitNumber		
                           ,@Area 
                           ,@FarmID
                           ,@OwnerID) 
                        SELECT * FROM Unit WHERE UnitID = SCOPE_IDENTITY()
                    ";
                }
                model = conn.Query<Unit>(sql, model).FirstOrDefault();
                return model;
            }
        }
        public void Delete(int UnitID)
        {
            using (var conn = Commons.DBContext) {
                conn.Execute(
                    "DELETE Unit WHERE UnitID=@UnitID ",
                    new { UnitID });
            }
        }
    }
}
