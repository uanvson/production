﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MvcPaging;
using Production.Business.Utilities;
using Production.DataAccess;

namespace Production.Business
{
    public class VarietyBL
    {
        public static List<Variety> GetList()
        {
            using (var conn = Commons.DBContext)
            {
                return conn.Query<Variety>(
                    "SELECT * FROM Variety"
                ).ToList();
            }
        }        

        public IPagedList<Variety> GetList(Variety model = null)
        {
            model = model ?? new Variety();
            using (var conn = Commons.DBContext) {
                string sqlWhere = "1=1";
                string sqlOrder = " VarietyID DESC";
                if (!string.IsNullOrEmpty(model.VarietyName))
                    sqlWhere += " AND VarietyName like '%' + @VarietyName+ '%'";
                if (!string.IsNullOrEmpty(model.SortBy))
                    sqlOrder = model.SortBy + " " + model.SortDirection;
                return conn.QueryPaging<Variety>(
                    "Variety.*, CustomerName",
                    "Variety LEFT JOIN Customer ON Variety.CustomerID = Customer.CustomerID",
                    sqlWhere,
                    sqlOrder,
                    model, model.Page - 1);

            }
        }
        public Variety GetById(int VarietyID)
        {
            using (var conn = Commons.DBContext) {
                return conn.Query<Variety>(
                    "SELECT * FROM Variety WHERE VarietyID=@VarietyID",
                    new { VarietyID })
                    .DefaultIfEmpty(new Variety())
                    .FirstOrDefault();
            }
        }
        public Variety Save(Variety model)
        {
            using (var conn = Commons.DBContext) {
                string sql = "";
                if (model.VarietyID > 0) {
                    sql = @"
                        UPDATE [Variety]
                        SET VarietyName	= @VarietyName		
                           ,VarietyCode	= @VarietyCode		
                           ,CustomerID     = @CustomerID
                        WHERE VarietyID = @VarietyID
                        SELECT * FROM Variety WHERE VarietyID =  @VarietyID
                    ";
                }
                else {
                    sql = @"
                        INSERT INTO [Variety]
                           ([VarietyName]
                           ,[VarietyCode]
                           ,CustomerID)
                     VALUES
                           (@VarietyName		
                           ,@VarietyCode		
                           ,@CustomerID) 
                        SELECT * FROM Variety WHERE VarietyID = SCOPE_IDENTITY()
                    ";
                }
                model = conn.Query<Variety>(sql, model).FirstOrDefault();
                return model;
            }
        }
        public void Delete(int VarietyID)
        {
            using (var conn = Commons.DBContext) {
                conn.Execute(
                    "DELETE Variety WHERE VarietyID=@VarietyID ",
                    new { VarietyID });
            }
        }
    }
}
