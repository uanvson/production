﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Production.Business
{
    public class Enums
    {
        public enum Language
        {
            English = 1,
            Farsi = 2,
            Spanish = 3,
            French = 4,
            Vietnamese = 5
        }

        public enum Permissions
        {
            Orders = 1,
            Tasks = 2,
            Customers = 3,
            Farms = 4,
            Units = 5,
            Crops = 6,
            Varieties = 7,
            Users = 8,
            Reports = 9,
        }
        public enum CookieType
        {
            Login,
            lang,
            dir,
            ShareId,
            Token,
            Tracking
        }

    }
}
