﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Production.Business.Utilities
{
    public class CookieUtilities
    {
        public static void DeleteCookie(string cookieKey)
        {
            if (HttpContext.Current == null)
                return;

            HttpCookie cookie = new HttpCookie(cookieKey.Trim().ToLower(), "");
            cookie.Expires = DateTime.Now.AddDays(-100);
            cookie.Path = "/";
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public static void DeleteCookie(Enums.CookieType cookieType)
        {
            DeleteCookie(cookieType.ToString());
        }

        public static void WriteCookie(string cookieKey, string cookieValue, int? days = null)
        {
            if (HttpContext.Current != null && !string.IsNullOrWhiteSpace(cookieKey))
            {
                
                cookieKey = cookieKey.Trim().ToLower();
                HttpCookie newCookie = GetCookie(cookieKey);

                if(newCookie == null)
                {
                    newCookie = new HttpCookie(cookieKey, cookieValue);
                    newCookie.Path = "/";
                    if (days.HasValue)
                    {
                        newCookie.Expires = DateTime.Now.AddDays(days.Value);
                    }
                    try
                    {
                        HttpContext.Current.Response.Cookies.Add(newCookie);
                    }
                    catch { }
                }else
                {
                    newCookie.Path = "/";
                    newCookie.Value = cookieValue;
                    if (days.HasValue)
                    {
                        newCookie.Expires = DateTime.Now.AddDays(days.Value);
                    }
                    try
                    {
                        HttpContext.Current.Response.Cookies.Set(newCookie);
                    }
                    catch { }
                    
                }
            }
        }
        public static void WriteCookie(Enums.CookieType cookieType, string cookieValue, int? days = null)
        {
            WriteCookie(cookieType.ToString(), cookieValue, days);
        }

        public static HttpCookie GetCookie(string cookieKey)
        {
            if (HttpContext.Current != null && !string.IsNullOrWhiteSpace(cookieKey))
            {
                cookieKey = cookieKey.Trim().ToLower();
                return HttpContext.Current.Request.Cookies[cookieKey];
            }

            return null;
        }

        public static string GetValue(string cookieKey)
        {
            string value = "";
            if (HttpContext.Current != null && !string.IsNullOrWhiteSpace(cookieKey))
            {
                cookieKey = cookieKey.Trim().ToLower();
                if (HttpContext.Current.Request.Cookies[cookieKey]!= null)
                value = HttpContext.Current.Request.Cookies[cookieKey].Value;
            }

            return value;
        }

        public static HttpCookie GetCookie(Enums.CookieType cookieType)
        {
            return GetCookie(cookieType.ToString());
        }

        public static bool CookieExists(string cookieKey)
        {
            return GetCookie(cookieKey) != null;
        }

        public static bool CookieExists(Enums.CookieType cookieType)
        {
            return GetCookie(cookieType) != null;
        }
    }
}
