﻿using Production.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Data.SqlClient;
using Dapper;
using System.Web.Routing;
using static Production.Business.Enums;

namespace Production.Business.Utilities
{
    public class SessionUtilities
    {

        public static Users CurrentUser
        {
            get
            {
                if (HttpContext.Current == null)
                    return new Users();

               string key = "CurrentUser";
               if (HttpContext.Current.Session[key] == null)
                {
                    HttpContext.Current.Session[key] = new Users();
                }

                return (Users)HttpContext.Current.Session[key];
            }

            set
            {
                string key = "CurrentUser";  
                HttpContext.Current.Session[key] = value;
            }
        }
        public static bool IsSuperAdmin()
        {
            return (CurrentUser != null && CurrentUser.RoleID == 3);
        }
        public static string UserPermission
        {
            get
            {
                string sessionKey = "UserPermission";
                if (HttpContext.Current.Session[sessionKey] == null) {
                    if (SessionUtilities.IsLoggedIn()) {
                        HttpContext.Current.Session[sessionKey] = getUserPermission();
                    }
                    else {
                        HttpContext.Current.Session[sessionKey] = string.Empty;
                    }
                }
                else {
                    if (!SessionUtilities.IsLoggedIn()) {
                        HttpContext.Current.Session[sessionKey] = string.Empty;
                    }
                }

                return (string)(HttpContext.Current.Session[sessionKey] == null ? "" : HttpContext.Current.Session[sessionKey]);
            }

            set
            {
                string sessionKey = "UserPermission";
                HttpContext.Current.Session[sessionKey] = value;
            }

        }
        public static bool HasPermission(Permissions permissionID)
        {

            if (SessionUtilities.UserPermission.ToString().IndexOf(";" + ((int)permissionID).ToString() + ";") != -1)
            {
                return true;
            }
            else
                return false;

        }
        public static string getUserPermission()
        {
            if (SessionUtilities.IsLoggedIn()) {
                var user = SessionUtilities.CurrentUser;
                using (var conn = Commons.DBContext) {
                    conn.Open();
                    string strPermission = "";
                    string sql = @"SELECT cast(PermissionID as nvarchar) 
                        FROM RolePermission 
                        WHERE RoleID = @RoleID";
                    if (SessionUtilities.IsSuperAdmin())
                        sql = @"SELECT cast(PermissionID as nvarchar) 
                        FROM Permission";
                    List<string> UserPermissionList = conn.Query<string>(sql
                       , new { CurrentUser.RoleID }).ToList();
                    foreach (var item in UserPermissionList) {
                        if (strPermission == "")
                            strPermission = item;
                        else
                            strPermission = strPermission + ";" + item;
                    }
                    return string.IsNullOrEmpty(strPermission) ? "" : ";" + strPermission + ";";
                }
            }
            else {
                return "";
            }

        }
        public static bool IsLoggedIn()
        {
            return (CurrentUser != null && CurrentUser.UserID > 0);
        }
        public static int CurrentLanguageID
        {
            get
            {
                
                string sessionKey = "Session_CurrentLanguageID";
                
                int sessionLangID = 2;
                if (HttpContext.Current.Session[sessionKey] != null) 
                    sessionLangID = (int)HttpContext.Current.Session[sessionKey];
                
                return sessionLangID;
            }

            set
            {
                string sessionKey = "Session_CurrentLanguageID";
                HttpContext.Current.Session[sessionKey] = value;
                
            }
        }

        
    }
}
