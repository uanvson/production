﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Production.Business.Utilities
{
    public class GlobalVariables
    {
        public static string DBConnectionString
        {
            get { return System.Configuration.ConfigurationManager.ConnectionStrings["production"].ConnectionString; }
        }
    }
}
