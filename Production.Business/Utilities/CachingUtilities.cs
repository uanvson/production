﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;

namespace Production.Business.Utilities
{
    public class CachingUtilities
    {
        private static object NullObject = new object();

        public static bool HasKey(string key)
        {
            bool result = false;

            if (HttpContext.Current == null)
                return false;

            var enumerable = HttpContext.Current.Cache.GetEnumerator();
            
            while (!result && enumerable.MoveNext())
            {
                result = enumerable.Key.ToString() == key;
            }
            return result;
        }

        
        public static object GetCachingValue(string key = null)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                return null;
            }

            
            if (HttpContext.Current == null)
            {
                return null;
            }
            Cache cache = HttpContext.Current.Cache;
            var result = cache[key.Trim()];
            return result == NullObject ? null : result;
        }

        public static void SetCachingValue(string key = null, object value = null, bool allowNull = false)
        {
            if (allowNull && value == null) value = NullObject;
            if (value != null)
            {
                
                if (HttpContext.Current == null)
                {
                    return;
                }

                Cache cache = HttpContext.Current.Cache;
                key = key.Trim();
                if (cache[key] != null)
                {
                    cache[key] = value;
                }
                else
                {
                    cache.Insert(key, value, null, DateTime.MaxValue, TimeSpan.FromSeconds(60 * 10)); 
                }
            }
        }

        public static void RemoveCache(string key)
        {
            if (HasKey(key))
            {
                Cache cache = HttpContext.Current.Cache;
                if (cache[key] != null)
                {
                    cache.Remove(key);
                }
            }
        }
    }
}
