﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Linq.Expressions;
using System.Dynamic;
using System.Collections;
using System.Web;

namespace Production.Business.Utilities
{
    public static class Commons
    {
        public static SqlConnection DBContext
        {
            get
            {
                return new SqlConnection(GlobalVariables.DBConnectionString);
            }
        }

        public static object Copy(dynamic source, object desc)
        {
            foreach (System.Reflection.PropertyInfo propertyInfo in desc.GetType().GetProperties())
            {
                try {
                    if (propertyInfo.CanWrite)
                    {

                        var value = source.GetType().GetProperty(propertyInfo.Name).GetValue(source);
                        propertyInfo.SetValue(desc, value);

                    }
                } catch(Exception ex) {}
                
            }
            return desc;
        }
        public static void ClearApplicationCache(List<string> containKeys = null)
        {
           
            var cache = HttpContext.Current.Cache;
            if (cache != null) {
                IDictionaryEnumerator enumerator = cache.GetEnumerator();
                List<string> keys = new List<string>();

                while (enumerator.MoveNext()) {
                    keys.Add(enumerator.Key.ToString());
                }

                // delete every key from cache
                for (int index = 0; index < keys.Count; index++) {
                    if (containKeys != null && containKeys.Count > 0) {
                        containKeys = (from c in containKeys
                                       where string.IsNullOrWhiteSpace(c) == false
                                       select c.ToLower().Trim()).ToList();

                        foreach (var removeKey in containKeys) {
                            string key = keys[index];
                            if (!string.IsNullOrWhiteSpace(key)) {
                                if (key.Trim().ToLower().Contains(removeKey.Trim().ToLower())) {
                                    cache.Remove(key);
                                }
                            }
                        }
                    }
                    else {
                        cache.Remove(keys[index]);
                    }
                }
            }
        }
    }

    
    


}
