﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Production.Business.Utilities;
using Production.DataAccess;

namespace Production.Business
{
    public class RoleBL
    {
        public List<Role> GetList(Role model = null)
        {
            model = model ?? new Role();
            using (var conn = Commons.DBContext)
            {
                string sql = "SELECT * FROM Role";
                if (!string.IsNullOrEmpty(model.RoleName))
                    sql += " WHERE RoleName like '%' + @RoleName+ '%'";
                if (!string.IsNullOrEmpty(model.SortBy))
                    sql += " ORDER BY " + model.SortBy + " " + model.SortDirection;
                return conn.Query<Role>(sql, model).ToList();
            }
        }
        public Role GetById(int RoleID)
        {
            using (var conn = Commons.DBContext) {
                var query = conn.Query<Role>(
                    "SELECT * FROM Role WHERE RoleID=@RoleID",
                    new { RoleID})
                    .DefaultIfEmpty(new Role())
                    .FirstOrDefault();
                query.Permissions = new PermissionBL().GetRolePermissions(RoleID);
                return query;
            }
        }
        public Role Save(Role model)
        {

            using (var conn = Commons.DBContext) {
                string sql = "";
                Role resut = new Role();
                if (model.RoleID > 0) {
                    sql = @"
                        UPDATE [Role]
                           SET [RoleName] = @RoleName
                              ,[Description] = @Description
                        WHERE RoleID = @RoleID
                        SELECT * FROM Role WHERE RoleID = @RoleID
                    ";
                }
                else {
                    sql = @"
                        INSERT INTO [Role]
                               ([RoleName]
                               ,[Description])
                         VALUES
                               (@RoleName
                               ,@Description)                         
                        SELECT * FROM Role WHERE RoleID = SCOPE_IDENTITY()
                    ";
                }
                resut = conn.Query<Role>(sql, model).FirstOrDefault();

                //Permissions
                sql = @"IF (NOT EXISTS(SELECT 1 FROM RolePermission WHERE RoleID = @RoleID AND PermissionID = @PermissionID))
                        INSERT INTO RolePermission(RoleID,PermissionID)
                        VALUES(@RoleID,@PermissionID) ";
                conn.Execute(sql, model.Permissions.Select(p=>new {model.RoleID,p.PermissionID }));

                sql = @"DELETE RolePermission 
                        WHERE PermissionID NOT IN @PermissionIDs
                        AND RoleID = @RoleID
                       ";
                conn.Execute(sql, new { model.RoleID, PermissionIDs = model.Permissions.Select(p => p.PermissionID) });
                return resut;
            }
        }
        public void Delete(int RoleID)
        {
            using (var conn = Commons.DBContext) {                
                conn.Execute(
                    "DELETE Role WHERE RoleID=@RoleID ", 
                    new { RoleID });
            }
        }
    }
}
