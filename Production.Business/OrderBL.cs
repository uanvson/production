﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Production.DataAccess;
using MvcPaging;
using Dapper;
using Production.Business.Utilities;
using System.Data.SqlClient;

namespace Production.Business
{
    public class OrderBL
    {
        public IPagedList<Order> GetList(Order order=null,string sortBy= "CreatedDate", string sortDirection="DESC", int page = 1)
        {
            order = order ?? new Order();
            using (var conn = new SqlConnection(GlobalVariables.DBConnectionString))
            {
                string sqlWhere = "1=1";
                if (order.CreatedDate != null)
                    sqlWhere += " AND DATEDIFF(day,(day,CreatedDate,@CreatedDate)=0 ";
                if (order.CustomerID > 0)
                    sqlWhere += " AND o.CustomerID = @CustomerID ";
                if (!string.IsNullOrEmpty(order.OrderNo))
                    sqlWhere += " AND OrderNo like '%' + @OrderNo + '%' ";

                return conn.QueryPaging<Order>(
                    "o.*, CustomerName",
                    "Orders o JOIN Customer c ON o.CustomerID = c.CustomerID ",
                    sqlWhere,
                    sortBy + " " + sortDirection, order, page - 1);
            }
            
        }
        public Order GetById(int OrderID)
        {
            using (var conn = Commons.DBContext)
            {
                return conn.Query<Order>("SELECT * FROM Orders WHERE OrderID = @OrderID",new { OrderID}).DefaultIfEmpty(new Order()).FirstOrDefault();
            }

        }
        public Order Save(Order model)
        {
            using (var conn = Commons.DBContext)
            {
                #region //Save order main
                string sql = "";
                if (model.OrderID > 0)
                {
                    sql = @"UPDATE Orders
                    SET	   OrderNo = @OrderNo
                          ,CreatedDate = @CreatedDate
                          ,CustomerID = @CustomerID
                          ,TotalQuantity = @TotalQuantity
                          ,TotalNumber = @TotalNumber
                          ,TotalHouse = @TotalHouse
                          ,UserID = @UserID
                          ,Supervisor = @Supervisor
                          ,Charge = @Charge
                          ,TotalMonney = @TotalMonney
                    WHERE OrderID = @OrderID 
                    SELECT * FROM Orders WHERE OrderID = @OrderID
                    ";
                }
                else
                {
                    sql = @"INSERT INTO Orders
                       (OrderNo
                       ,CreatedDate
                       ,CustomerID
                       ,TotalQuantity
                       ,TotalNumber
                       ,TotalHouse
                       ,UserID
                       ,Supervisor
                       ,Charge
                       ,TotalMonney)
                 VALUES
                       (@OrderNo
                       ,@CreatedDate
                       ,@CustomerID
                       ,@TotalQuantity
                       ,@TotalNumber
                       ,@TotalHouse
                       ,@UserID
                       ,@Supervisor
                       ,@Charge
                       ,@TotalMonney) 
                    SELECT * FROM Orders WHERE OrderID = SCOPE_IDENTITY() ";
                }
                model = conn.Query<Order>(sql, model).FirstOrDefault();
                #endregion

                //Save order items
                SaveDetails(model.OrderDetails);

                return model;
            }

        }
        public List<dynamic> GetDetails(int orderId)
        {
            using (var conn = Commons.DBContext)
            {
                List<OrderDetail> list = new List<OrderDetail>();
                var query = conn.QueryPaging<dynamic>(
                    @"d.*, VarietyName,VarietyCode,v.InternalCode,PlantBase,QualityGrade,
                      Crop.CropCode ",
                    @"OrderDetail d 
                      JOIN Variety v ON d.VarietyID = v.VarietyID 
                      JOIN Crop ON Crop.CropID = d.CropID ",
                    " d.OrderID=@OrderID",
                    " ID DESC", new { orderId }, 0 , 0).ToList();
                return query;
            }
        }
        public OrderDetail GetDetailById(OrderDetail detail)
        {
            using (var conn = Commons.DBContext)
            {
                string sql = @"
                    SELECT * FROM OrderDetail d 
                    JOIN Crop ON Crop.CropID = d.CropID 
                    WHERE d.ID=@ID 
                ";
                detail = conn.Query<OrderDetail>(sql, new { ID = detail.ID }).DefaultIfEmpty(new OrderDetail()).FirstOrDefault();
                if (detail != null)
                    detail.Variety = new VarietyBL().GetById(detail.VarietyID.GetValueOrDefault());
                
                return detail;
            }
        }
        public void DeleteDetail(int Id)
        {
            using (var conn = Commons.DBContext)
            {
                conn.Execute("DELETE OrderDetail WHERE ID=@ID", new { Id});
            }
        }
        public void SaveDetails(List<OrderDetail> details)
        {
            using (var conn = Commons.DBContext)
            {
                string sql = "";
                foreach(var model in details)
                {
                    if (model.ID == 0)
                    {
                        sql = @"INSERT INTO [OrderDetail]
                               ([OrderID]
                               ,[CropID]
                               ,[VarietyID]
                               ,[Hourse]
                               ,[Quantity]
                               ,[PlantNumber]
                               ,[DateSownFemale]
                               ,[DateSownMale]
                               ,[DeliveryDate]
                               ,[SeedMale]
                               ,[SeedFemale]
                               ,[Remark])
                         VALUES
                               (@OrderID
                                @CropID
                                @VarietyID
                                @Hourse
                                @Quantity
                                @PlantNumber
                                @DateSownFemale
                                @DateSownMale
                                @DeliveryDate
                                @SeedMale
                                @SeedFemale
                                @Remark) 
                         --SELECT * FROM OrderDetail WHERE ID=SCOPE_IDENTITY() ";
                    }
                    else
                    {
                        sql = @"UPDATE [OrderDetail]
                               ([OrderID]
                               ,[CropID]
                               ,[VarietyID]
                               ,[Hourse]
                               ,[Quantity]
                               ,[PlantNumber]
                               ,[DateSownFemale]
                               ,[DateSownMale]
                               ,[DeliveryDate]
                               ,[SeedMale]
                               ,[SeedFemale]
                               ,[Remark])
                         SET
                                OrderID=@OrderID
                                ,CropID=@CropID
                                ,VarietyID=@VarietyID
                                ,Hourse=@Hourse
                                ,Quantity=@Quantity
                                ,PlantNumber=@PlantNumber
                                ,DateSownFemale=@DateSownFemale
                                ,DateSownMale=@DateSownMale
                                ,DeliveryDate=@DeliveryDate
                                ,SeedMale=@SeedMale
                                ,SeedFemale=@SeedFemale
                                ,Remark=@Remark
                        WHERE ID = @ID
                        --SELECT * FROM OrderDetail WHERE ID = @ID  ";
                    }
                    conn.Execute(sql, model);

                }
            }
        }
        public OrderDetail SaveDetail(OrderDetail model)
        {
            
            using (var conn = Commons.DBContext)
            {
                string sql = "";
                //foreach (var model in details)
                {
                    if (model.ID == 0)
                    {
                        sql = @"INSERT INTO OrderDetail
                               (OrderID
                               ,CropID
                               ,VarietyID
                               ,House
                               ,Quantity
                               ,PlantNumber
                               ,MalePlantNumber
                               ,FemalePlantNumber
                               ,SowDate
                               ,DeliveryDate
                               ,MaleHouseNumber
                               ,FemaleHouseNumber
                               ,Remark)
                         VALUES
                               (@OrderID
                               ,@CropID
                               ,@VarietyID
                               ,@House
                               ,@Quantity
                               ,@PlantNumber
                               ,@MalePlantNumber
                               ,@FemalePlantNumber
                               ,@SowDate
                               ,@DeliveryDate
                               ,@MaleHouseNumber
                               ,@FemaleHouseNumber
                               ,@Remark) 
                         SELECT * FROM OrderDetail WHERE ID=SCOPE_IDENTITY() ";
                    }
                    else
                    {
                        sql = @"UPDATE [OrderDetail]                             
                         SET
                                OrderID              = @OrderID
                               ,CropID               = @CropID
                               ,VarietyID            = @VarietyID
                               ,House                = @House
                               ,Quantity             = @Quantity    
                               ,PlantNumber          = @PlantNumber
                               ,MalePlantNumber      = @MalePlantNumber
                               ,FemalePlantNumber    = @FemalePlantNumber
                               ,SowDate              = @SowDate
                               ,DeliveryDate         = @DeliveryDate
                               ,MaleHouseNumber      = @MaleHouseNumber
                               ,FemaleHouseNumber    = @FemaleHouseNumber
                               ,Remark               = @Remark 
                        WHERE ID = @ID
                        SELECT * FROM OrderDetail WHERE ID = @ID  ";
                    }
                    return conn.Query<OrderDetail>(sql, model).FirstOrDefault();

                }
            }
        }

    }
}
