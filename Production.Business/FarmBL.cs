﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Production.DataAccess;
using Production.Business.Utilities;
using MvcPaging;

namespace Production.Business
{
    public class FarmBL
    {
        static public List<Farm> GetList()
        {
            using (var conn = Commons.DBContext)
            {
                return conn.Query<Farm>("SELECT * FROM Farm").ToList();
            }
        }

        public IPagedList<Farm> GetList(Farm model = null)
        {
            model = model ?? new Farm();
            using (var conn = Commons.DBContext) {
                string sqlWhere = "1=1";
                string sqlOrder = " FarmID DESC";
                if (!string.IsNullOrEmpty(model.FarmName))
                    sqlWhere += " AND FarmName like '%' + @FarmName+ '%'";
                if (!string.IsNullOrEmpty(model.SortBy))
                    sqlOrder =  model.SortBy + " " + model.SortDirection;
                return conn.QueryPaging<Farm>(
                    "Farm.*,FirstName ",
                    "Farm LEFT JOIN Users ON Farm.OwnerID = Users.UserID",
                    sqlWhere,
                    sqlOrder,
                    model, model.Page-1);

            }
        }
        public Farm GetById(int FarmID)
        {
            using (var conn = Commons.DBContext) {
                return conn.Query<Farm>(
                    "SELECT * FROM Farm WHERE FarmID=@FarmID",
                    new { FarmID })
                    .DefaultIfEmpty(new Farm())
                    .FirstOrDefault();
            }
        }
        public Farm Save(Farm model)
        {
            using (var conn = Commons.DBContext) {
                string sql = "";
                if (model.FarmID > 0) {
                    sql = @"
                        UPDATE [Farm]
                        SET FarmName	= @FarmName		
                           ,FarmNumber	= @FarmNumber		
                           ,Area		= @Area			
                           ,Address	    = @Address			
                           ,Phone		= @Phone			
                           ,Email		= @Email			
                           ,CreatedDate = @CreatedDate		
                           ,OwnerID     = @OwnerID
                        WHERE FarmID = @FarmID
                        SELECT * FROM Farm WHERE FarmID =  @FarmID
                    ";
                }
                else {
                    sql = @"
                        INSERT INTO [Farm]
                           ([FarmName]
                           ,[FarmNumber]
                           ,[Area]
                           ,[Address]
                           ,[Phone]
                           ,[Email]
                           ,[CreatedDate]
                           ,[OwnerID])
                     VALUES
                           (@FarmName		
                           ,@FarmNumber		
                           ,@Area			
                           ,@Address			
                           ,@Phone			
                           ,@Email			
                           ,@CreatedDate		
                           ,@OwnerID) 
                        SELECT * FROM Farm WHERE FarmID = SCOPE_IDENTITY()
                    ";
                }
                model = conn.Query<Farm>(sql, model).FirstOrDefault();
                return model;
            }
        }
        public void Delete(int FarmID)
        {
            using (var conn = Commons.DBContext) {
                conn.Execute(
                    "DELETE Farm WHERE FarmID=@FarmID ",
                    new { FarmID });
            }
        }

    }
}
