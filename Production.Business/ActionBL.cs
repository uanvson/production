﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Production.DataAccess;
using Production.Business.Utilities;

using Dapper;
using MvcPaging;
using AutoMapper;

namespace Production.Business
{
    public class ActionBL
    {
        public static List<ActionMaster> GetMasterList()
        {
            using (var conn = Commons.DBContext)
            {
                var list = conn.Query<ActionMaster>("SELECT * FROM ActionMaster").ToList();
                foreach(var act in list) {
                    act.ActionName = ResourceBL.GetText(act.ActionName);
                }
                return list;
            }
        }
        public IPagedList<Actions> GetList(Actions action,string sortBy= "p.CreatedDate", string sortDirection = "", int page = 1)
        {
            action = action ?? new Actions();
            using (var conn = Commons.DBContext)
            {
                string sqlWhere = "1=1";
                if (action.ActionID > 0)
                    sqlWhere += " AND p.ActionID=@ActionID";
                if (action.Order.CustomerID > 0)
                    sqlWhere += " AND o.CustomerID=@CustomerID";
                if (!string.IsNullOrEmpty(action.Order.OrderNo) )
                    sqlWhere += " AND o.OrderNo like '%' + @OrderNo +'%'";
                if (action.CreatedDate != null)
                    sqlWhere += " AND DATEDIFF(day,(day,CreatedDate,@CreatedDate)=0 ";

                var query = conn.QueryPaging<dynamic>(
                    "p.*, ActionName, VarietyCode,VarietyName, OrderNo",
                    @"Action p 
                    JOIN ActionMaster m ON m.ActionID = p.ActionID
                    LEFT JOIN Variety v ON v.VarietyID = p.VarietyID
                    LEFT JOIN Orders o ON o.OrderID = p.OrderID
                    ",
                    sqlWhere,"p.CreatedDate",
                    new {
                        action.ActionID,
                        action.CreatedDate,
                        action.Order.CustomerID,
                        action.Order.OrderNo
                    }, page-1);

                var actions = query.Select(p => new Actions()
                {
                    ID = p.ID,
                    CreatedDate = p.CreatedDate,
                    ActionName = p.ActionName,
                    Variety = new Variety()
                    {
                        VarietyCode = p.VarietyCode,
                        VarietyName = p.VarietyName
                    },
                    Order = new Order()
                    {
                        OrderNo = p.OrderNo
                    }
                });

                return actions.ToPagedList(query.PageIndex, query.PageSize, query.TotalItemCount);
            }
        }
        public Actions GetById(Actions action)
        {
            string sql =
                    @"SELECT p.*,ActionName,FarmNumber,
                    u.FirstName,u.LastName,VarietyCode, OrderNo
                    FROM Action p 
                    JOIN ActionMaster m ON m.ActionID = p.ActionID
                    LEFT JOIN Farm ON Farm.FarmID = p.FarmID
                    LEFT JOIN Users u ON u.UserID = p.OwnerID   
                    LEFT JOIN Variety v ON v.VarietyID = p.VarietyID
                    LEFT JOIN Orders o ON o.OrderID = p.OrderID
                    WHERE p.ID =@ID";
            using (var conn = Commons.DBContext) {
                var query = conn.Query<dynamic>(sql, action).FirstOrDefault();              

                if (query != null)
                    //action = AutoMapper.Mapper.Map<Actions>(query);
                    action = new Actions(){
                        ID = query.ID,
                        ActionID = query.ActionID,
                        CreatedDate = query.CreatedDate,
                        ActionName = query.ActionName,
                        FarmID = query.FarmID,
                        VarietyID = query.VarietyID,
                        OrderID = query.OrderID ?? 0,
                        StepNumber = query.StepNumber,
                        OwnerID = query.OwnerID ?? 0,
                        CropID = query.CropID ?? 0,
                        Farm = new Farm()
                        {
                            FarmID = query.FarmID,
                            FarmNumber = query.FarmNumber                            
                        },
                        Variety = new Variety()
                        {
                            VarietyID = query.VarietyID,
                            VarietyName = query.VarietyName
                        },
                        Order = new Order()
                        {
                            OrderID = query.OrderID,
                            OrderNo = query.OrderNo
                        },
                        Owner = new Users()
                        {
                            UserID = query.UserID,
                            FirstName = query.FirstName,
                            LastName = query.LastName
                        },
                        PlantNumber = query.PlantNumber,
                        HouseNumber = query.HouseNumber,
                        Note = query.Note,

                    };
                return action;
            }
        
        }
        public Actions Save(Actions model)
        {
            using (var conn = Commons.DBContext) {

                string sql = "";
                if (model.ID > 0) {
                    sql = @"UPDATE [Action]
                           SET [ActionID] = @ActionID
                              ,[FarmID] = @FarmID
                              ,[OwnerID] = @OwnerID
                              ,[HouseNumber] = @HouseNumber
                              ,[HourseIDs] = @HourseIDs
                              ,[CropID] = @CropID
                              ,[VarietyID] = @VarietyID
                              ,[PlantNumber] = @PlantNumber
                              ,[StepNumber] = @StepNumber
                              ,[OrderID] = @OrderID
                              ,[ActionDate] = @ActionDate
                              ,[DeliveryDate] = @DeliveryDate
                              ,[SownMale] = @SownMale
                              ,[SownFemale] = @SownFemale
                              ,[Tray] = @Tray
                              ,[UnitID] = @UnitID
                              ,[Area] = @Area
                              ,[Note] = @Note
                              ,[UserID] = @UserID
                              ,[Status] = @Status
                              ,[CreatedDate] = @CreatedDate
                         WHERE ID= @ID 
                    SELECT * FROM Action WHERE ID = @ID
                    ";
                }
                else {
                    sql = @"INSERT INTO [Action]
                           ([ActionID]
                           ,[FarmID]
                           ,[OwnerID]
                           ,[HouseNumber]
                           ,[HourseIDs]
                           ,[CropID]
                           ,[VarietyID]
                           ,[PlantNumber]
                           ,[StepNumber]
                           ,[OrderID]
                           ,[ActionDate]
                           ,[DeliveryDate]
                           ,[SownMale]
                           ,[SownFemale]
                           ,[Tray]
                           ,[UnitID]
                           ,[Area]
                           ,[Note]
                           ,[UserID]
                           ,[Status]
                           ,[CreatedDate])
                     VALUES
                            (
                            @ActionID
                           ,@FarmID
                           ,@OwnerID
                           ,@HouseNumber
                           ,@HourseIDs
                           ,@CropID
                           ,@VarietyID
                           ,@PlantNumber
                           ,@StepNumber
                           ,@OrderID
                           ,@ActionDate
                           ,@DeliveryDate
                           ,@SownMale
                           ,@SownFemale
                           ,@Tray
                           ,@UnitID
                           ,@Area
                           ,@Note
                           ,@UserID
                           ,@Status
                           ,@CreatedDate) 
                    SELECT * FROM Action WHERE ID = SCOPE_IDENTITY() ";
                }
                model = conn.Query<Actions>(sql, model).FirstOrDefault();

                return model;
            }

        }
        public void Delete(int Id)
        {
            using (var conn = Commons.DBContext) {
                conn.Execute("DELETE Action WHERE ID=@ID", new { Id });
            }
        }

    }
}
