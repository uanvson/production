﻿namespace Production.DataAccess
{   
   
    using System;
    using System.Collections.Generic;

    public partial class Resource
    {
        public int ResourceID { get; set; }
        public string ResourceKey { get; set; }
        public int LanguageID { get; set; }
        public string Value { get; set; }
    }
   
}
