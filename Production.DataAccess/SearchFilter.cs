﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Production.DataAccess
{
    public class SearchFilter
    {
        public SearchFilter()
        {
            Page = 1;
        }
        public string SortBy { get; set; }
        public string SortDirection { get; set; }
        public int Page { get; set; }
    }
}
