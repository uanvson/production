﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Production.DataAccess
{
    [Serializable()]
    public class LoggedUser
    {
        public int UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int CustomerID { get; set; }
        public int UnitID { get; set; }
        public int RoleID { get; set; }
        public int LanguageID { get; set; }
    }
}
