//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Production.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Permission: SearchFilter
    {
        public int PermissionID { get; set; }
        public int OldPermissionID { get; set; }
        public string PermissionName { get; set; }
        public string Description { get; set; }
        public string IsCreate { get; set; }
    }
}
