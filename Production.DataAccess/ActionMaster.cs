﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Production.DataAccess
{
    public class ActionMaster
    {
        public int ActionID { get; set; }
        public string ActionName { get; set; }
    }
}
