﻿$(document).ready(function () {
    PressEnter();

    
    

})


$(document).on('show.bs.modal', '.modal', function () {
    var zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function () {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});

$(document).ready(function () {
    
    $.ajaxSetup({ cache: false });

    // Ajax activity indicator bound to ajax start/stop document events
    $(document).ajaxStart(function () {

    })
    .ajaxSend(function (e, request, settings) {        

        // show loading icon if request is not in ignore action list
        var ignoreActions = [                     
        ];
        var requestUrl = settings.url.toLowerCase();
        var arr = jQuery.grep(ignoreActions, function (action) {
            return requestUrl.indexOf(action) > -1; // match action
        });
        var showLoadingIcon = arr.length == 0; // request action is ignore or not.
        if (showLoadingIcon) {
            fadeIn();
        }
    })
    .ajaxStop(function (response) {
        //fadeOut();
        //hideLoading();        
    });

    $(document).ajaxComplete(function (event, xhr, settings) {

        if (settings.url.indexOf("tools/getadhocmailstatus") == -1) {
            fadeOut();
            //hideLoading();
        }        
    });

    if (ValidatorMessages) {
        $.extend($.validator.messages, {
            required: ValidatorMessages.required,
            email: ValidatorMessages.email,
            url: ValidatorMessages.url,
            date: ValidatorMessages.date,
            digits: ValidatorMessages.digits,
            safe: ValidatorMessages.safe
        });
    }

});

function showMessage(params) {    
    params.title = params.title ? params.title : "Message";
    if ($("#dvMessage").length == 0)
        $("body").append("<div style='display:none' id='dvMessage'></div>");    
    $("#dvMessage").html(params.content);
    $("#dvMessage").popupModal(params);
}
function showDialog(messge, title) {
    showMessage({ title:title, content: messge});    
}

function showModal(params) {
    var options = $.extend({
        containerID: "popupContainer",
        container: null,
        dialogClass: "", // modal-sm, modal-lg
        fullscreen: "", // modal-sm, modal-lg
        showCloseButton: true,
        showOKButton: false,
        openEffect: true,
        backdrop: 'static',
        keyboard: false,
        shouldReplace: false,
        destroyOnClose: false
    }, params);
    var popupContainer = null;
    var contentObj = null;
    if (options.container) {
        if (options.container.hasClass("modal"))
            popupContainer = options.container;
        else
            contentObj = options.container;
    }
    if (!popupContainer) {
        if (!contentObj)
            contentObj = getObjectByID(options.containerID, false, { isShow: true });

        if (contentObj.length > 0 && contentObj.hasClass("modal")) {
            popupContainer = contentObj;
        }
        else {
            // build html modal
            if (contentObj.length === 0) {
                if (options.containerID) {
                    contentObj = getObjectByID(options.containerID, true, { isShow: true });
                    contentObj.html(options.content ? options.content : "");
                }
                else {
                    contentObj = options.content ? options.content : "";
                }
            }

            var hasUniqueID = options.shouldReplace ? false : true;
            var modalID = hasUniqueID ? "popupContainer_" + Math.random().toString(36).substr(2, 9) : "popupContainer";
            var htmlCloseButton = "";
            if (options.showCloseButton) {
                htmlCloseButton = "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>";
            }
            var htmlFooter = "";
            if (options.footerContent || options.showOKButton) {
                htmlFooter = "<div class=\"modal-footer\" >" +
                    (options.footerContent ? options.footerContent : "") +
                    (options.showOKButton ? "<button type=\"button\" class='btn btn-sm btn-success' data-dismiss=\"modal\">OK</button>" : "") +
                    "</div>";
            }

            var htmlModal = $("<div id=\"" + modalID + "\" class=\"modal " + options.fullscreen + (options.openEffect ? " fade " : "") + "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">" +
                "<div class=\"modal-dialog " + options.dialogClass + " \" role=\"document\">" +
                "<div class=\"modal-content\"> " +
                "<div class=\"modal-header\" >" +
                "<h4 class=\"modal-title\" id=\"title_" + modalID + "\">" + (options.title ? options.title : "") + "</h4>" +
                htmlCloseButton +
                "</div>" +
                "<div class=\"modal-body\" ></div>" +
                htmlFooter +
                "</div>" +
                "</div>" +
                "</div>");

            popupContainer = $(htmlModal);

            popupContainer.appendTo("body")
                .find(".modal-body")
                .append(contentObj);
        }
    }

    // events
    popupContainer.on('show.bs.modal', function (e) {
        console.log("show");
        if (options.onBeforeShow) options.onBeforeShow(e);
    });

    popupContainer.on('shown.bs.modal', function (e) {
        console.log("shown!");
        if (options.onShow) options.onShow(e);
    });

    popupContainer.on('hide.bs.modal', function (e) {
        if (options.onHide) options.onHide(e);
    });
    popupContainer.on('hidden.bs.modal', function (e) {
        if (options.destroyOnClose) {
            popupContainer.data("bs.modal", null);
            popupContainer.detach();
            popupContainer.remove();
        }
    });

    // override
    if (options.title) {
        popupContainer.find(".modal-title").text(options.title);
    }

    contentObj.show();
    popupContainer.modal();

    return popupContainer;
}

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
jQuery.fn.extend({
    popupModal: function (params) {
        if (typeof params === "string") {
            var modalObj = null;
            var obj = $(this);
            if (obj.hasClass("modal"))
                modalObj = obj;
            else
                modalObj = obj.closest(".modal");

            if (modalObj.length > 0) {
                try {
                    modalObj.modal(params);
                }
                catch (ex) {
                    console.log(ex);
                }
            }
            return;
        }
        var options = $.extend({}, params);
        options.container = $(this);
        return showModal(options);
    }
});

jQuery.fn.extend({
    dialog: function (options, attr, val) {
        var id = this.selector;
        dialog(id, options, attr, val);
        return $(id);
    }
});

function dialog(id, options, attr, val) {

    //if (!options) {
    //    options = { };
    //}

    var uWidth = (options && options.width) ? (";width:" + (options.width > window.screen.width ? window.screen.width - 13 : options.width) + "px") : "";
    var uHeight = (options && options.height) ? (";height:" + options.height + "px") : "";
    var uTitle = (options && options.title) ? (options.title) : "";
    var uClose = (options && options.close) ? (options.close) : "";
    var dgID = "dg" + id.replace("#", "").replace(".", "");
    var footerContent = (options && options.footerContent) ? (options.footerContent) : "";
    var customClass = (options && options.customClass) ? (options.customClass) : "";
    var modalFooter = "";
    if (footerContent.length > 0) {
        modalFooter = "<div class=\"modal-footer\" >" + footerContent + "</div>";
    }

    if (options == "destroy") {
        $("#" + dgID).data("bs.modal", null)
        $("#" + dgID).detach();
        return;
    }

    var modalClass = (options && options.lgClass) ? "<div class=\"modal-dialog modal-lg\" > " : "<div class=\"modal-dialog " + (options && options.dSize ? options.dSize : "") + "\" > ";
    var fullScreenModalClass = (options && options.fullScreen) ? "modal-fullscreen" : "";

    //if (typeof options.openEffect == 'undefined' || options.openEffect == null) {
    //    options.openEffect = "";
    //}

    var htmlColseButton = "<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>";
    if (options && options.showCloseButton == false) {
        htmlColseButton = "";
    }
    if ($("#" + dgID).length == 0) {
        var dg = $("<div class=\"modal " + customClass + " " + (options && options.openEffect ? " fade " : "") + fullScreenModalClass + "\" id=\"" + dgID + "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">" +
		  modalClass +
			"<div class=\"modal-content\"  > " +
			  "<div class=\"modal-header\" >" +
				htmlColseButton +
				"<h4 class=\"modal-title\" id=\"Label" + dgID + "\">" + uTitle + "</h4>" +
                "<div style=\"clear:both\"></div>" +
			  "</div>" +
			  "<div class=\"modal-body\" >" +
			  "</div>" +
              modalFooter +
			"</div>" +
		  "</div>" +
		"</div>");
        //$(id).replaceWith(dg);

        $("body:first").append(dg);
        $("#" + dgID + " .modal-body").wrapInner($(id));

        if (options == "option" && attr == "title" && val)
            $("#Label" + dgID).html(val);

        $("#" + dgID).on('show.bs.modal', function (e) {
            // fix ios safari address bar overlapping issue
            //if ($("#" + dgID + " .modal-dialog").offset().top < 70) {
            //    $("#" + dgID + " .modal-dialog").css("top", "70px");
            //}
            // end: fix ios safari address bar overlapping issue
            if (options && options.closeEffect) {
                $("#" + dgID + " .modal-dialog").removeClass(options.closeEffect);
            }
            if (options && options.openEffect) {
                $("#" + dgID + " .modal-dialog")
                        .addClass(options.openEffect)
                        .addClass("animated");
            }
            if (options && options.show)
                options.show();
        });

        $("#" + dgID).on('hide.bs.modal', function (e) {
            if (options && options.openEffect) {
                $("#" + dgID + " .modal-dialog").removeClass(options.openEffect);
            }
            if (options && options.closeEffect) {
                $("#" + dgID + " .modal-dialog")
                    .addClass(options.closeEffect)
                    .addClass("animated");
            }
        });

        $("#" + dgID).on('hidden.bs.modal', function () {
            //$("#" + dgID).replaceWith($(id));
            // clear animate classes
            //$("#" + dgID + " .modal-dialog")
            //.removeClass("zoomIn")
            //.removeClass("zoomOut")
            //.removeClass("animated");

            if (options && options.close)
                options.close();
            if (options && options.stop)
                $(this).remove();
            $(id).hide();
            if (options && options.destroyOnClose) {
                $("#" + dgID).data("bs.modal", null)
                $("#" + dgID).detach();
            }
        });

        //$("#" + dgID).on("shown.bs.modal", alignModal);

        $("#" + dgID).modal({
            backdrop: 'static',
            keyboard: false,
            show: false
        });
    }
    else {
        if (options && options.title)
            $('.modal-title', $("#" + dgID)).html(options.title);
    }

    $("[data-toggle='tooltip']", $("#" + dgID)).tooltip();
    if (options == "close") {
        $("#" + dgID).modal('hide');
        //$(id).hide();
    }
    else if (options == "open" || !options || (options && options.autoOpen != false)) {
        //$("#" + dgID).css("z-index", $(".modal.in").length * 10 + 5000);
        if ($('.modal:visible').length) {
            // has visible modal
            var zIndex = Math.max.apply(null, Array.prototype.map.call(document.querySelectorAll('*'), function (el) {
                return el.style.zIndex;
            }));

            if ($("#" + dgID).css('z-index') < zIndex) {
                $("#" + dgID).css('z-index', zIndex + 10);
            }

            // Align modal when user resize the window
            //$(window).on("resize", function () {
            //    $(".modal:visible").each(alignModal);
            //});
        }
        $("#" + dgID).modal('show');
        $(id).show();
        if (parent != self) parent.postMessage("scrollIframeTop", "*");

        if (isAppleDevice()) {
            //$(".modal-dialog").css("top", $.cookie("off") + "px");

        }
    }

}

function PressEnter() {
    $(".pressenter, .pressenter input, .pressenter select").unbind("keypress");
    $(".pressenter, .pressenter input, .pressenter select").keypress(function (event) {
        var charCode = event.keyCode || event.which;
        if (charCode == 13) {
            var handler = $(this).attr("enter-handler") != undefined ? $(this).attr("enter-handler") : $(this).attr("enterhandler");
            if (handler)
                eval(handler);
        }
    });
}
function fadeIn(immediate, processingData, isManual) {
    if (!immediate)
        immediate = false;

    var hasProcessingData = processingData && processingData.percent;
    if ($(".fade-in-processing").length == 0) {
        var dvLoading = "<div class='fade-in-processing' style='z-index:9999; position:fixed; width:100%;height:100%; padding-top: 30%;padding-left:50%;left: 0;top: 0;background-color:transparent; opacity:1;filter:alpha(opacity=1)'>";
        dvLoading += "<div class='loading-box'>";
        dvLoading += "<i style='font-size: 3.0em; color:#333; left: 50%;top: 50%; transform: translate(-50%, -50%);' class='fa fa-spinner fa-spin'></i>";
        dvLoading += "</div></div>";
        $("body").append(dvLoading);
    }

    if (hasProcessingData) {
        $(".fade-in-processing .loading-box").addClass("bg-lightgray");
    }
    else {
        $(".fade-in-processing .loading-box").removeClass("bg-lightgray");
    }

    if (!immediate)
        $(".fade-in-processing").fadeIn();
    else
        $(".fade-in-processing").show();

    if (processingData && processingData.percent) {
        if ($(".fade-in-processing .loading-box .process-info").length == 0) {
            $(".fade-in-processing .loading-box").append("<div class='process-info'></div>");
        }
        $(".fade-in-processing .loading-box .process-info").html("Processing " + processingData.percent + "% - " + processingData.rowIndex + "/" + processingData.totalRow);
    }
    else {
        if ($(".fade-in-processing .loading-box .process-info").length > 0) {
            $(".fade-in-processing .loading-box .process-info").remove();
        }
    }

    if (isManual) {
        window.isProcessing = true;
    }

    $(".ajaxloading").show();
}

function fadeOut(immediate, isManual) {
    if (window.isProcessing === true) {
        if (isManual !== true) return;
        window.isProcessing = false;
    }

    if (!immediate)
        immediate = false;

    if (!immediate)
        $(".fade-in-processing").fadeOut();
    else
        $(".fade-in-processing").hide();


    $(".ajaxloading").hide();

    if ($(".fade-in-processing .loading-box .process-info").length > 0) {
        $(".fade-in-processing .loading-box .process-info").remove();
    }
}

//$(".modal").on("shown.bs.modal", function () {
//    if ($(".modal-backdrop").length > 1) {
//        $(".modal-backdrop").not(':first').remove();
//    }
//})

function changeCurrentLanguage(languageId) {
    $.ajax({
        type: "POST",
        url: SiteConfig.Prefix + "/Home/ChangeCurrentLanguage",
        data: { languageId: languageId, returnURL: window.location.href },
        success: function (data) {
            if (data.result == "ok") {
                window.location.href = data.returnURL;
            }
        }
    });
}