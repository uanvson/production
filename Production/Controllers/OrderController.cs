﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Production.Business.Utilities;
using Production.DataAccess;
using Production.Business;

namespace production.Controllers
{
    public class OrderController : Controller
    {
        //
        // GET: /Order/
        OrderBL orderBL = new OrderBL();
        CustomerBL customerBL = new CustomerBL();

        public ActionResult Index()
        {
            ViewBag.customers = customerBL.GetList(new Customer());
            return View();
        }
        public ActionResult _Orders(Order order, string sortBy="OrderID", string sortDirection="DESC", int page = 1)
        {

            var orders = orderBL.GetList(order, sortBy,sortDirection, page);
            return PartialView(orders);
            
        }
        public ActionResult Edit(int OrderID = 0)
        {
            var model = orderBL.GetById(OrderID);
            ViewBag.customers = customerBL.GetList();
            return PartialView(model);
        }
        public ActionResult Details(int OrderID = 0)
        {
            var model = orderBL.GetDetails(OrderID);
            return PartialView(model);
        }
        public ActionResult EditDetail(OrderDetail detail)
        {
            var model = orderBL.GetDetailById(detail);
            ViewBag.Crops = CropBL.GetList();
            ViewBag.Varieties = VarietyBL.GetList();
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult SaveDetail(OrderDetail model)
        {
            model = orderBL.SaveDetail(model);
            return Json(model);
        }

        [HttpPost]
        public ActionResult Save(Order model)
        {
            if (model.OrderID == 0)
            {
                model.UserID = SessionUtilities.CurrentUser.UserID;
                model.CreatedDate = DateTime.Now;
            }                

            model = orderBL.Save(model);            
            return Json(model);
        }
        [HttpPost]
        public ActionResult DeleteDetail(int Id)
        {
            orderBL.DeleteDetail(Id);
            return Json(new { result="ok",message=""});
        }
        public ActionResult GetVariety(int varietyId=0)
        {
            var model = new VarietyBL().GetById(varietyId);
            return Json(model,JsonRequestBehavior.AllowGet);
        }
    }
}
