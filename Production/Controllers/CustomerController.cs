﻿using Production.Business;
using Production.Business.Utilities;
using Production.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace production.Controllers
{
    public class CustomerController : Controller
    {
        CustomerBL customerBL = new CustomerBL();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _List(Customer model)
        {
            var list = customerBL.GetList(model);
            return PartialView(list);
        }
        public ActionResult Edit(int CustomerID)
        {
            Customer cust = customerBL.GetById(CustomerID);
            return PartialView(cust);
        }
        public ActionResult Save(Customer model)
        {
            return Json(customerBL.Save(model));
        }
        public ActionResult Delete(int CustomerID)
        {
            customerBL.Delete(CustomerID);
            return Json(new {result="ok" });
        }

    }
}
