﻿using Production.Business;
using Production.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace production.Controllers
{
    public class CropController : Controller
    {
        CropBL cropBL = new CropBL();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _List(Crop model)
        {
            var list = cropBL.GetList(model);
            return PartialView(list);
        }
        public ActionResult Edit(int CropID)
        {
            ViewBag.Customers = new CustomerBL().GetList();
            Crop cust = cropBL.GetById(CropID);
            return PartialView(cust);
        }
        public ActionResult Save(Crop model)
        {
            return Json(cropBL.Save(model));
        }
        public ActionResult Delete(int CropID)
        {
            cropBL.Delete(CropID);
            return Json(new { result = "ok" });
        }

    }
}
