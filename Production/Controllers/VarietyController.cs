﻿using Production.Business;
using Production.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace production.Controllers
{
    public class VarietyController : Controller
    {
        VarietyBL VarietyBL = new VarietyBL();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _List(Variety model)
        {
            var list = VarietyBL.GetList(model);
            return PartialView(list);
        }
        public ActionResult Edit(int VarietyID)
        {
            ViewBag.Customers = new CustomerBL().GetList();
            Variety cust = VarietyBL.GetById(VarietyID);
            return PartialView(cust);
        }
        public ActionResult Save(Variety model)
        {
            return Json(VarietyBL.Save(model));
        }
        public ActionResult Delete(int VarietyID)
        {
            VarietyBL.Delete(VarietyID);
            return Json(new { result = "ok" });
        }

    }
}
