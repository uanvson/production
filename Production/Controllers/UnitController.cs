﻿using Production.Business;
using Production.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace production.Controllers
{
    public class UnitController : Controller
    {
        UnitBL UnitBL = new UnitBL();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _List(Unit model)
        {
            var list = UnitBL.GetList(model);
            return PartialView(list);
        }
        public ActionResult Edit(int UnitID)
        {
            ViewBag.Farms = FarmBL.GetList();
            ViewBag.Users = UsersBL.GetList();
            Unit cust = UnitBL.GetById(UnitID);
            return PartialView(cust);
        }
        public ActionResult Save(Unit model)
        {
            return Json(UnitBL.Save(model));
        }
        public ActionResult Delete(int UnitID)
        {
            UnitBL.Delete(UnitID);
            return Json(new { result = "ok" });
        }

    }
}
