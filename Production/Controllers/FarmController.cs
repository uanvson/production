﻿using Production.Business;
using Production.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace production.Controllers
{
    public class FarmController : Controller
    {
        FarmBL farmBL = new FarmBL();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _List(Farm model)
        {
            var list = farmBL.GetList(model);
            return PartialView(list);
        }
        public ActionResult Edit(int FarmID)
        {
            Farm cust = farmBL.GetById(FarmID);
            return PartialView(cust);
        }
        public ActionResult Save(Farm model)
        {
            return Json(farmBL.Save(model));
        }
        public ActionResult Delete(int FarmID)
        {
            farmBL.Delete(FarmID);
            return Json(new { result = "ok" });
        }

    }
}
