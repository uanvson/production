﻿using Production.Business;
using Production.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace production.Controllers
{
    public class PermissionController : Controller
    {
        PermissionBL PermissionBL = new PermissionBL();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _List(Permission model)
        {
            var list = PermissionBL.GetList(model);
            return PartialView(list);
        }
        public ActionResult Edit(int PermissionID)
        {
            Permission cust = PermissionBL.GetById(PermissionID);
            return PartialView(cust);
        }
        public ActionResult Save(Permission model)
        {
            var data = PermissionBL.Save(model);
            if (data.PermissionID == -1)
                return Json(new { result = "failed", message = "PermissionID duplicated." });

            return Json(new { result = "ok"});
        }
        public ActionResult Delete(int PermissionID)
        {
            PermissionBL.Delete(PermissionID);
            return Json(new { result = "ok" });
        }

    }
}
