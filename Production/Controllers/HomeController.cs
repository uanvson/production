﻿using Production.Business;
using Production.Business.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace production.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
       
        public ActionResult Index()
        {
           
            return RedirectToAction("", "Order");
            //return View();
        }

        public ActionResult ChangeCurrentLanguage(int languageId, string returnURL)
        {
            
                if (SessionUtilities.CurrentLanguageID != languageId) {
                    SessionUtilities.CurrentLanguageID = languageId;
                
                CookieUtilities.WriteCookie("lang", languageId.ToString(), 365);
                Commons.ClearApplicationCache();
            }

            return Json(new { result = "ok", returnURL }, JsonRequestBehavior.AllowGet);
        }

    }
}
