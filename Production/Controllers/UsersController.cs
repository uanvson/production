﻿using Production.Business;
using Production.Business.Utilities;
using Production.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace production.Controllers
{
    public class UsersController : Controller
    {
        UsersBL UsersBL = new UsersBL();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _List(Users model)
        {
            var list = UsersBL.GetList(model);
            return PartialView(list);
        }
        public ActionResult Edit(int UserID)
        {
            ViewBag.Roles = new RoleBL().GetList();
            ViewBag.Customers = new CustomerBL().GetList();
            Users user = UsersBL.GetById(UserID);
            return PartialView(user);
        }
        public ActionResult Save(Users model)
        {
            return Json(UsersBL.Save(model));
        }
        public ActionResult Delete(int UserID)
        {
            UsersBL.Delete(UserID);
            return Json(new { result = "ok" });
        }
        public ActionResult Login(Users model)
        {
            return View();
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Login", "Users");
        }
        public ActionResult UserLogin(Users model)
        {
            string result = "";
            Users user = UsersBL.GetByEmail(model.Email);
            if (user != null) {
                Session["CurrentUser"] = user;
                Session["UserPermission"] = SessionUtilities.getUserPermission();
                result = "ok";
            }
            else {
                result = "failed";
            }
            return Json(new { result }, JsonRequestBehavior.AllowGet);
        }

    }
}
