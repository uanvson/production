﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Production.Business.Utilities;
using Production.DataAccess;
using Production.Business;
using System.Web.Caching;
using System.Collections;

namespace production.Controllers
{
    
    public class ActionsController : Controller
    {
        ActionBL actionBL = new ActionBL();
        CustomerBL customerBL = new CustomerBL();
        public ActionResult Index()
        {
            ViewBag.actions = ActionBL.GetMasterList();
            ViewBag.customers = customerBL.GetList();
            return View();
        }
        public ActionResult _List(Actions act, Order filter)
        {
            act.Order = filter;
            var actions = actionBL.GetList(act);
            return PartialView(actions);
        }

        public ActionResult Edit(Actions act)
        {
            ViewBag.Actions = ActionBL.GetMasterList();
            ViewBag.Farms = FarmBL.GetList();
            ViewBag.Units = UnitBL.GetList();
            ViewBag.Crops = CropBL.GetList();
            ViewBag.Varieties = VarietyBL.GetList();
            ViewBag.Orders = new OrderBL().GetList();
            var actions = actionBL.GetById(act);
            return PartialView(actions);
        }
        public ActionResult ClearCache()
        {            
            foreach (DictionaryEntry entry in HttpContext.Cache) {
                HttpContext.Cache.Remove(entry.Key.ToString());
            }
            return Json(new {result="ok"},JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult Save(Actions model)
        {
            if (model.ID == 0) {
                model.UserID = SessionUtilities.CurrentUser.UserID;
                model.CreatedDate = DateTime.Now;
            }
            else {
                model.CreatedDate = model.CreatedDate ?? DateTime.Now;
                model.UserID = SessionUtilities.CurrentUser.UserID;
            }


            model = actionBL.Save(model);
            return Json(model);
        }
        public ActionResult Delete(int Id)
        {
            actionBL.Delete(Id);
            return Json(new { result = "ok", message = "" });
        }


    }
}
