﻿using Production.Business;
using Production.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace production.Controllers
{
    public class RoleController : Controller
    {
        RoleBL RoleBL = new RoleBL();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult _List(Role model)
        {
            var list = RoleBL.GetList(model);
            return PartialView(list);
        }
        public ActionResult Edit(int RoleID)
        {
            ViewBag.Permissions = PermissionBL.GetList();
            Role cust = RoleBL.GetById(RoleID);
            return PartialView(cust);
        }
        public ActionResult Save(Role model)
        {
            return Json(RoleBL.Save(model));
        }
        public ActionResult Delete(int RoleID)
        {
            RoleBL.Delete(RoleID);
            return Json(new { result = "ok" });
        }

    }
}
